<!doctype HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="stylesheet" href="https://swiperjs.com/package/swiper-bundle.min.css">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
        <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.7.2/aframe/build/aframe-ar.js"></script>
        <script src="js/swiped-events.min.js"></script>
        <script src="https://unpkg.com/aframe-event-set-component@^4.0.0/dist/aframe-event-set-component.min.js"></script>
        <script src="https://rawgit.com/mayognaise/aframe-html-shader/master/dist/aframe-html-shader.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400&amp;display=swap" rel="stylesheet">
        <?php
            $types = ["flor", "prerolled", "hash"];

            $total = [
                "flor" => 0, 
                "prerolled" => 0, 
                "hash" => 0
            ];

            foreach ($types as $type) {
                $elements = glob('models/{*.glb}',GLOB_BRACE);

                foreach($elements as $element) {
                    if(strpos($element, $type) !== false) {
                        $total[$type]++;
                    }
                }

            }
        ?>

        <script type="module" src="https://unpkg.com/ionicons@5.1.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule="" src="https://unpkg.com/ionicons@5.1.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" href="assets/css/font-awesome-4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <script src="dist/aframe-preloader-component.min.js"></script>
        <script>

            var visualizador = false;
            var pagina = 1;
            var objetos_weed = 1;
            var max_objetos_weed = 1;
            var objetos_hash = 1;
            var max_objetos_hash = 1;
            var objetos_prerolled = 1;
            var max_objetos_prerolled = 1;

            max_objetos_weed = <?php echo $total["flor"]; ?>;
            objetos_weed = 1;

            max_objetos_hash = <?php echo $total["hash"]; ?>;
            objetos_hash = 1;
            
            max_objetos_prerolled = <?php echo $total["prerolled"]; ?>;
            objetos_prerolled = 1;

            AFRAME.registerComponent('registerevents', {
                init: function () {
                    var marker = this.el;

                    marker.addEventListener('markerFound', function() {
                    
                        document.getElementById('menu').style.opacity = 1;
                        if (visualizador == true){
                            document.getElementById('hmtl_elements').style.opacity = 1;
                        }
                        document.getElementById('scan_marker_ux').style.opacity = 0;
                    });

                    marker.addEventListener('markerLost', function() {
                        document.getElementById('menu').style.opacity = 0;
                        document.getElementById('hmtl_elements').style.opacity = 0;
                        document.getElementById('scan_marker_ux').style.opacity = 1;
                    });
                }
            });

            AFRAME.registerComponent('loader', {
                init: function() {

                    document.addEventListener('swiped-left', function(e) {
                        swap('left');
                    });

                    document.addEventListener('swiped-right', function(e) {
                        swap("right")
                    });
                }
            });

            function swap(orientacion) {
                    var type = "";
                    var objetos = 0;
                    var max_objetos = 0;
                    var infotype_1 = "";
                    var type_1 = "";

                    if (pagina == 1){
                        type = "flor";
                        objetos = objetos_weed;
                        max_objetos = max_objetos_weed;
                        type_1 = document.querySelector("#glbtest_weed");
                        infotype_1 = document.querySelector("#textweed");
                    } else if (pagina == 2){
                        type = "hash";
                        objetos = objetos_hash;
                        max_objetos = max_objetos_hash;
                        type_1 = document.querySelector("#glbtest_hash");
                        infotype_1 = document.querySelector("#texthash");
                    } else if (pagina == 3){
                        type = "prerolled";
                        objetos = objetos_prerolled;
                        max_objetos = max_objetos_prerolled;
                        type_1 = document.querySelector("#glbtest_prerolled");
                        infotype_1 = document.querySelector("#textprerolled");
                    }

                    modeloBase = "models/" + type;
                    infoBase = "img/info" + type;

                    if (orientacion == 'left') {
                        objetos++;

                        if (objetos >= max_objetos) {
                            objetos = 1;
                        }
                    } else {
                        console.log(objetos);
                        console.log(max_objetos);
                        objetos--;
                        if (objetos <= 0) {
                            objetos = max_objetos;
                        }
                    }



                    type_1.setAttribute("gltf-model", modeloBase + objetos +  ".glb");

                    infotype_1.setAttribute("src", infoBase + objetos + ".jpg");
                
                    if (pagina == 1){
                        objetos_weed = objetos;
                    } else if (pagina == 2){
                        objetos_hash = objetos;
                    } else if (pagina == 3){
                        objetos_prerolled = objetos;
                    }

                    $("#pagina").html(objetos);
            }

            AFRAME.registerComponent('button', {
                init: function() {

                    const button1 = document.querySelector('#button_weed');
                    const button2 = document.querySelector('#button_hash');
                    const button3 = document.querySelector('#button_prerolled');

                    const button_rotate = document.querySelector('#button_rotate');
                    const img_gh = document.querySelector('#img_gh');
                    var rotate = true;

                    const logo = document.querySelector('#logo');
                    const logo_neon = document.querySelector('#logo_neon');
                    const pageweed = document.querySelector('#weed_page');
                    const paginacion = document.querySelector('#hmtl_elements');

                    button1.addEventListener('click', function(){
                        logo.setAttribute('scale', '0 0 0');
                        logo_neon.setAttribute('scale', '0 0 0');
                        weed_page.setAttribute('scale', '1 1 1');
                        hash_page.setAttribute('scale', '0 0 0');
                        prerolled_page.setAttribute('scale', '0 0 0');
                        paginacion.style.opacity = 1;
                        visualizador = true;
                        pagina = 1;
                        max_objetos_weed = <?php echo $total["flor"]; ?>;
                        objetos_weed = 1;
                        $("#pagina").html(objetos_weed);
                        $("#pagina_max").html(max_objetos_weed);
                    });

                    button2.addEventListener('click', function(){
                        logo.setAttribute('scale', '0 0 0');
                        logo_neon.setAttribute('scale', '0 0 0');
                        hash_page.setAttribute('scale', '1 1 1');
                        weed_page.setAttribute('scale', '0 0 0');
                        prerolled_page.setAttribute('scale', '0 0 0');
                        paginacion.style.opacity = 1;
                        visualizador = true;
                        pagina = 2;
                        max_objetos_hash = <?php echo $total["hash"]; ?>;
                        objetos_hash = 1;
                        $("#pagina").html(objetos_hash);
                        $("#pagina_max").html(max_objetos_hash);
                    });

                    button3.addEventListener('click', function(){
                        logo.setAttribute('scale', '0 0 0');
                        logo_neon.setAttribute('scale', '0 0 0');
                        weed_page.setAttribute('scale', '0 0 0');
                        hash_page.setAttribute('scale', '0 0 0');
                        prerolled_page.setAttribute('scale', '1 1 1');
                        paginacion.style.opacity = 1;
                        visualizador = true;
                        pagina = 3;
                        max_objetos_prerolled = <?php echo $total["prerolled"]; ?>;
                        objetos_prerolled = 1;
                        $("#pagina").html(objetos_prerolled);
                        $("#pagina_max").html(max_objetos_prerolled);
                    });

                    button_rotate.addEventListener('click', () => {

                        if(rotate){

                            logo.setAttribute('rotation', '-90 0 0');
                            logo_neon.setAttribute('rotation', '-90 0 0');
                            img_gh.setAttribute('rotation', '-90 0 0');
                            weed_page.setAttribute('rotation', '-70 0 0');
                            weed_page.setAttribute('position', '0 0.60 1');
                            hash_page.setAttribute('rotation', '-70 0 0');
                            hash_page.setAttribute('position', '0 0.60 1');
                            prerolled_page.setAttribute('rotation', '-70 0 0');
                            prerolled_page.setAttribute('position', '0 0.60 1');

                            rotate = false;

                        }else{

                            logo.setAttribute('rotation', '0 0 0');
                            logo_neon.setAttribute('rotation', '0 0 0');
                            img_gh.setAttribute('rotation', '0 0 0');
                            weed_page.setAttribute('rotation', '0 0 0');
                            weed_page.setAttribute('position', '0 0 0');
                            hash_page.setAttribute('rotation', '0 0 0');
                            hash_page.setAttribute('position', '0 0 0');
                            prerolled_page.setAttribute('rotation', '0 0 0');
                            prerolled_page.setAttribute('position', '0 0 0');
                            
                            rotate = true;
                        }                 
                    })
                }


            });

        </script>
    </head>
    
    <body data-swipe-threshold="100">

        <div id="preloader-modal" class="modal instructions-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-dialog__full" role="document">
            <div class="modal-content vertical-align text-center">
              <div class="col-md-6 col-md-offset-3">
              <div class="row">
                  <img id="logo_preloader" src="img/logo_gh.png">
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <i class="preloader-modal__ok fa fa-check fa-5x text-success" aria-hidden="true"></i>
                    <i class="preloader-modal__spinner fa fa-cog fa-spin fa-5x fa-fw text-muted" aria-hidden="true"></i>
                  </div>
                </div>
                <div class="row loading_message">
                  <div class="col-xs-12">
                    <span class="progress-label">Loading 0%</span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <button type="button" class="button_gh sbtn btn-default clearfix" data-dismiss="modal">Start</button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 powered">
                    <p>Powered by</p>
                    <img src="img/logo-retina.png">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="hmtl_elements">
            <div id="paginacion">
                <span id="pagina"></span>
                <span> / </span>
                <span id="pagina_max"></span>
            </div>
            <div id="izq" onclick="izquierda()"><img src="img/flecha_izq.svg"></div>
            <div id="drx" onclick="derecha()"><img src="img/flecha_drx.svg"></div>
        </div>
        <div id="menu">
            <ul style="display: none;">
                <li>
                    <a id="link_rotate">
                        <ion-icon name="reload-circle" id="button_rotate"></ion-icon>
                    </a>
                </li>
            </ul>
            <ul>
                <li><a id="button_weed" class="button_gh"></a></li>
                <li><a id="button_hash" class="button_gh"></a></li>
                <li><a id="button_prerolled" class="button_gh"></a></li>
            </ul>
        </div>

        <div id="scan_marker_ux">
            <img id="scan_marker_img" src="img/scan_marker.png">
        </div>

        <a-scene embedded arjs='sourceType: webcam; debugUIEnabled: false;'
                 loading-screen="enabled: false"
                 device-orientation-permission-ui="enabled:false;"
                 vr-mode-ui="enabled:false;"
                 preloader="autoInject: false; clickToClose: true; autoClose: true; target: #preloader-modal; bar: #preloader-modal .progress-bar; label: #preloader-modal .progress-label; labelText: Loading {0}%; slowLoad: true; doneLabelText: Press Start To Play!;"
                 loader>

            <a-assets>
                <?php

                    foreach ($total as $type => $totalElement) {

                        for ($i=1; $i <= $totalElement ; $i++) { 
                            
                            echo '<a-asset-item id="model'.$i.'" src="models/'.$type . $i.'.glb" preload="auto"></a-asset-item>';
                            echo '<img id="textura'.$i.'" src="models/'.$type.$i.'_u0_v0.jpeg" preload="auto"></img>';
                            echo '<img id="infografia'.$i.'" src="img/info'.$type.$i.'.jpg" preload="auto"></img>';
                        }

                    }
                ?>
            </a-assets>

            <a-marker id="anchor" emitevents="true" button registerevents type="pattern" url="assets/marker/marker.patt">

                <a-entity cursor="rayOrigin: mouse; fuse: false; downEvents:  ;  upEvents:  "
                     raycaster="objects:  .clickable;  direction:  0 0 -1;  origin:  0 0 0;  useWorldCoordinates:  true; near: 0"
                     device-orientation-permission-ui="enabled"></a-entity>

                <a-entity obj-model="obj: neon/texto.obj" scale="0.1 0.1 0.1" material="color: #fff01f; emissive: #ffee80; emissiveIntensity: 0.21; metalness: 0.42; roughness: 0.48" id ="logo" position=" -0.5 2.5 -1.5"></a-entity>
                <a-entity obj-model="obj: neon/neon.obj" scale="0.1 0.1 0.1" material="src: neon/brillo.png; transparent: true; side: double" id ="logo_neon" position="-0.5 2.5 -1.5"></a-entity>

                <a-image id="img_gh" src="img/25.png" scale="2 2 0.67" class="clickable" rotation="-90 0 0" gesture-handler="" material="" geometry="primitive: box" position="-0.5 0.33 -0.5"></a-image>
                
                
                <a-entity scale="0 0 0" id="weed_page" position="-0.5 0.74 -0.5">
                    
                    <a-entity id="glbtest_weed" gltf-model="models/flor1.glb" position="0.1 0 0.064" scale="0.12 0.12 0.12" animation__rotation="property: rotation; from: 0 0 0; to: 0 360 0; easing:linear; loop: true; delay: 5000; dur:  14000; loop:  999; ">
                    </a-entity>

                    <a-entity>
                        <a-image id="textweed" src="img/infoflor1.jpg" scale="1.5 1.5 1.5" position="-0.61 1.9 -1.3" visible="" material="" geometry="width: 1; height: 2.54" rotation="-15 15 0"></a-image>
                    </a-entity>

                </a-entity>

                <a-entity scale="0 0 0" id="hash_page" position="-0.5 0.74 -0.5">
                    
                    <a-entity id="glbtest_hash" gltf-model="models/hash1.glb" position="0 0 0" scale="0.12 0.12 0.12" animation__rotation="property: rotation; from: 0 0 0; to: 0 360 0; easing:linear; loop: true; delay: 5000; dur:  14000; loop:  999; ">
                    </a-entity>

                    <a-entity>
                        <a-image id="texthash" src="img/infohash1.jpg" scale="1.5 1.5 1.5" position="0 1.3 -1.5" visible="" material="" geometry="width: 2.8; height: 1" rotation="-35 0 0"></a-image>
                    </a-entity>

                </a-entity>

                <a-entity scale="0 0 0" id="prerolled_page" position="-0.5 0.74 -0.5">
                    
                    <a-entity id="glbtest_prerolled" gltf-model="models/prerolled1.glb" position="0 1.33 0" scale="1 1 1" animation__rotation="property: rotation; from: 0 0 0; to: 0 360 0; easing:linear; loop: true; delay: 5000; dur:  14000; loop:  999; ">
                    </a-entity>

                    <a-entity>
                        <a-image id="textprerolled" src="img/infoprerolled1.jpg" scale="1.5 1.5 1.5" position="-0.61 1.9 -1.3" visible="" material="" geometry="width: 1; height: 2.54" rotation="-15 15 0"></a-image>
                    </a-entity>

                </a-entity>

            </a-marker>

            <a-entity id="rotationfixer" position="0 0 0" rotation="">
                <a-camera camera="fov: 44; zoom: 0"
                    id="camera" position="0 0 0"
                    wasd-controls-enabled="false"
                    look-controls="enabled: false"
                    rotation=""
                    wasd-controls=""
                    zoom="1.5987500000000001"
                    data-aframe-inspector-original-camera=""
                    raycaster="objects: .clickable; useWorldCoordinates: false; direction: 0 0 0; near: 1.8;"
                    cursor="rayOrigin: mouse; fuse: false; downEvents:  ;  upEvents:  "></a-camera>
            </a-entity>
        
        </a-scene>

        <script>
            function derecha(){
                const swipeleft = new Event('swiped-left');    
                document.dispatchEvent(swipeleft);
            }
            
            function izquierda(){
                const swiperight = new Event('swiped-right');    
                document.dispatchEvent(swiperight);
            }
        </script>
    </body>
</html>